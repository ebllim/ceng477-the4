#version 410

layout(location = 0) in vec3 position;

// Data from CPU
uniform mat4 MVP; // ModelViewProjection Matrix
uniform mat4 MV; // ModelView idMVPMatrix
uniform mat4 inverseTransposeV;
uniform vec4 cameraPosition;
uniform vec4 lightPosition;
uniform float heightFactor;

// Texture-related data
uniform sampler2D rgbTexture;
uniform int widthTexture;
uniform int heightTexture;


// Output to Fragment Shader
out vec2 textureCoordinate; // For texture-color
out vec3 vertexNormal; // For Lighting computation
out vec3 ToLightVector; // Vector from Vertex to Light;
out vec3 ToCameraVector; // Vector from Vertex to Camera;


void main()
{

    // get texture value, compute height
    textureCoordinate = vec2(1 - (position.x / widthTexture), 1 - (position.z / heightTexture));
    vec4 textureColor = texture(rgbTexture, textureCoordinate);
    float y = heightFactor * (0.2126 * textureColor.r + 0.7152 * textureColor.g + 0.0722 * textureColor.b);
    vec4 v = vec4(position.x, y,position.z, 1);

    vec2 tmpTC;
    vec4 tmpTColor;
    float tmpy;

    int neighbourCount = 0;
    int triangleCount = 0;
    vec4 n0 = vec4(0,0,0,1);
    vec4 n1 = vec4(0,0,0,1);
    vec4 n2 = vec4(0,0,0,1);
    vec4 n3 = vec4(0,0,0,1);
    vec4 n4 = vec4(0,0,0,1);
    vec4 n5 = vec4(0,0,0,1);

    if(v.x == 0 && v.y == 0){
        neighbourCount = 3;
        triangleCount = 2;

        n0 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x + 1, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);
    }
    else if(v.x == widthTexture && v.z == heightTexture){
        neighbourCount = 3;
        triangleCount = 2;

        n0 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x - 1, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);
    }
    else if(v.x == widthTexture && v.z == 0){
        neighbourCount = 2;
        triangleCount = 1;

        n0 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

    }
    else if(v.x == 0 && v.z == heightTexture){
        neighbourCount = 2;
        triangleCount = 1;

        n0 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);
    }
    else if(v.x == 0){
        neighbourCount = 4;
        triangleCount = 3;

        n0 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x + 1, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n3 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n3.x / widthTexture), 1 - (n3.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n3.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

    }
    else if(v.x == widthTexture){
        neighbourCount = 4;
        triangleCount = 3;

        n0 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x - 1, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n3 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n3.x / widthTexture), 1 - (n3.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n3.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);
    }
    else if(v.z == 0){
        neighbourCount = 4;
        triangleCount = 3;

        n0 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x + 1, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n3 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n3.x / widthTexture), 1 - (n3.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n3.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

    }
    else if(v.z == heightTexture){
        neighbourCount = 4;
        triangleCount = 3;

        n0 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x - 1, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n3 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n3.x / widthTexture), 1 - (n3.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n3.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);
    }
    else{
        n0 = vec4(v.x - 1, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n0.x / widthTexture), 1 - (n0.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n0.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n1 = vec4(v.x - 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n1.x / widthTexture), 1 - (n1.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n1.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n2 = vec4(v.x, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n2.x / widthTexture), 1 - (n2.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n2.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n3 = vec4(v.x + 1, 0, v.z + 1, 1);
        tmpTC = vec2(1 - (n3.x / widthTexture), 1 - (n3.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n3.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n4 = vec4(v.x + 1, 0, v.z, 1);
        tmpTC = vec2(1 - (n4.x / widthTexture), 1 - (n4.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n4.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

        n5 = vec4(v.x, 0, v.z - 1, 1);
        tmpTC = vec2(1 - (n5.x / widthTexture), 1 - (n5.z / heightTexture));
        tmpTColor = texture(rgbTexture, tmpTC);
        n5.y = heightFactor * (0.2126 * tmpTColor.r + 0.7152 * tmpTColor.g + 0.0722 * tmpTColor.b);

    }

    //Avarage here
    vec3 norm = vec3(0,0,0);
    norm = norm +  cross(vec3(n0-v), vec3(n1-v));
    norm = norm +  cross(vec3(n1-v), vec3(n2-v));

    if(triangleCount > 2){
        norm = norm +  cross(vec3(n2-v), vec3(n3-v));
    }
    if(triangleCount > 3){
        norm = norm +  cross(vec3(n3-v), vec3(n4-v));
    }
    if(triangleCount > 4){
        norm = norm +  cross(vec3(n4-v), vec3(n5-v));
        norm = norm +  cross(vec3(n5-v), vec3(n0-v));
    }


    norm = normalize(norm);
    vertexNormal = normalize(vec3( transpose(inverse(MV)) * vec4(norm,0) ));




    // compute toLight vector & toCamera vector vertex coordinate in VCS
    ToLightVector = normalize(vec3(MV * vec4(lightPosition.xyz - v.xyz,0)));
    ToCameraVector = normalize(vec3(MV * vec4(cameraPosition.xyz - v.xyz,0)));


    // set gl_Position variable correctly to give the transformed vertex position
    gl_Position = MVP * v;
}
