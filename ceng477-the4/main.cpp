#include <ostream>
#include "helper.h"
#include "glm/glm.hpp"
#include "glm/ext.hpp"
//#include "glm/gtx/rotate_vector.hpp"
//#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"

#define SPEEDSTEP 0.25f
#define HEIGHTFACTORSTEP 0.5f
#define PITCHSTEP 0.05f
#define YAWSTEP 0.05f

static GLFWwindow *win = NULL;

GLFWvidmode *mode;
const GLFWmonitor *monitor;
bool isFullScreen = false;
// Shaders
GLuint idProgramShader;
GLuint idFragmentShader;
GLuint idVertexShader;
GLuint idJpegTexture;
GLuint idMVPMatrix;

int widthTexture, heightTexture;
int widthImagePlane, heightImagePlane;
int widthImagePlaneLast, heightImagePlaneLast;
int g=0;

static void errorCallback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

class Plane {
public:
    glm::vec3 position;
    glm::vec3 up;
    glm::vec3 gaze;
    glm::vec3 u;

    float heightFactor;
    float ppAngle;
    float ppAspectRatio;
    float speed;
    float nearDistance;
    float farDistance;

    glm::mat4 modelM;
    glm::mat4 viewM;
    glm::mat4 projectionM;
    glm::mat4 mv;
    glm::mat4 mvp;

    Plane() {
        this->position = glm::vec3(widthTexture / 2, widthTexture / 10, -(widthTexture / 4));
        this->gaze = glm::vec3(0.0, 0.0, 1.0);
        this->up = glm::vec3(0.0, 1.0, 0.0);
        this->u = glm::vec3(-1.0, 0.0, 0.0);

        this->heightFactor = 10.0;
        this->ppAngle = 45.0;
        this->ppAspectRatio = 1;
        this->speed = 0.0;
        this->nearDistance = 0.1;
        this->farDistance = 1000.0;
    }


    void move() {
        this->position = this->position + this->speed * this->gaze;
        this->setShaderData();

    }

    void pitch(int mode) {
        glm::mat4 rotationM;
        if (mode) {
            rotationM = glm::rotate(glm::radians(PITCHSTEP), this->u);
//            rotationM = glm::rotate(PITCHSTEP, this->u);
        } else {
            rotationM = glm::rotate(glm::radians(-PITCHSTEP), this->u);
//            rotationM = glm::rotate(-PITCHSTEP, this->u);
        }
        this->gaze = glm::vec3(rotationM * glm::vec4(this->gaze, 1));
        this->gaze = glm::normalize(this->gaze);

        this->up = glm::vec3(rotationM * glm::vec4(this->up, 1));
        this->up = glm::normalize(this->up);

//        this->fixCameraVectors();
    }

    void yaw(int mode) {
        glm::mat4 rotationM;
        if (mode) {
            rotationM = glm::rotate(glm::radians(YAWSTEP), this->up);
//            rotationM = glm::rotate(YAWSTEP, this->up);
        } else {
            rotationM = glm::rotate(glm::radians(-YAWSTEP), this->up);
//            rotationM = glm::rotate(-YAWSTEP, this->up);
        }

        this->gaze = glm::vec3(rotationM * glm::vec4(this->gaze, 1));
        this->gaze = glm::normalize(this->gaze);

        this->u = glm::vec3(rotationM * glm::vec4(this->u, 1));
        this->u = glm::normalize(this->u);

//        this->fixCameraVectors();
    }

    void fixCameraVectors() {
        this->u = glm::normalize(glm::cross(this->up, this->gaze));
//        this->up = glm::normalize(glm::cross(-this->gaze, this->u));
    }

    void changeSpeed(int mode) {
        if (mode) {
            this->speed += SPEEDSTEP;
        } else {
            this->speed -= SPEEDSTEP;
        }
    }

    void changeHeightFactor(int mode) {
        if (mode) {
            this->heightFactor += HEIGHTFACTORSTEP;
        } else {
            this->heightFactor -= HEIGHTFACTORSTEP;
        }
    }

    void setShaderData() {
        glViewport(0, 0, widthImagePlane, heightImagePlane);

        this->projectionM = glm::perspective(
                this->ppAngle,
                this->ppAspectRatio,
                this->nearDistance,
                this->farDistance
        );

        this->viewM = glm::lookAt(
                this->position,
                glm::vec3(
                        this->position.x + this->gaze.x * this->nearDistance,
                        this->position.y + this->gaze.y * this->nearDistance,
                        this->position.z + this->gaze.z * this->nearDistance
                ),
                this->up
        );

        this->modelM = glm::mat4(1.0f);

        this->mv = this->viewM * this->modelM;
        this->mvp = this->projectionM * this->viewM * this->modelM;

        GLint shaderLocation = glGetUniformLocation(idProgramShader, "MV");
        glUniformMatrix4fv(shaderLocation, 1, GL_FALSE, &mv[0][0]);

        shaderLocation = glGetUniformLocation(idProgramShader, "MVP");
        glUniformMatrix4fv(shaderLocation, 1, GL_FALSE, &mvp[0][0]);

        glm::vec4 lightPosition4 = glm::vec4(
                (float) widthTexture / 2.0,
                (float) widthTexture + (float) heightTexture,
                (float) heightTexture / 2.0,
                1);
        shaderLocation = glGetUniformLocation(idProgramShader, "lightPosition");
        glUniform4fv(shaderLocation, 1, &lightPosition4.x);

        glm::vec4 cameraPosition4 = glm::vec4(this->position, 1);
        shaderLocation = glGetUniformLocation(idProgramShader, "cameraPosition");
        glUniform4fv(shaderLocation, 1, &cameraPosition4.x);

        shaderLocation = glGetUniformLocation(idProgramShader, "heightFactor");
        glUniform1f(shaderLocation, this->heightFactor);

        shaderLocation = glGetUniformLocation(idProgramShader, "widthTexture");
        glUniform1i(shaderLocation, widthTexture);

        shaderLocation = glGetUniformLocation(idProgramShader, "heightTexture");
        glUniform1i(shaderLocation, heightTexture);

    }
};

Plane *plane;
glm::vec3 *vertices;
int triangleCount;

void prepareVertexData(int w, int h) {
    triangleCount = 2 * w * h;
    vertices = new glm::vec3[triangleCount * 3];
    glm::vec3 vertex1, vertex2, vertex3, vertex4;
    int k = 0;
    for (int i = 0; i < w-1; ++i) {
        for (int j = 0; j < h-1; ++j) {
            vertex1 = glm::vec3(i, 0, j);
            vertex2 = glm::vec3(i + 1, 0, j);
            vertex3 = glm::vec3(i, 0, j + 1);
            vertex4 = glm::vec3(i + 1, 0, j + 1);

            vertices[k++] = vertex1;
            vertices[k++] = vertex4;
            vertices[k++] = vertex2;

            vertices[k++] = vertex1;
            vertices[k++] = vertex3;
            vertices[k++] = vertex4;

        }
    }


}

void renderFunction() {
    glClearColor(0, 0, 0, 1);
    glClearDepth(1.0f);
    glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, vertices);

    glDrawArrays(GL_TRIANGLES, 0, triangleCount * 3);

    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays

}


static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, GLFW_TRUE);
                break;
            case GLFW_KEY_W:   // Change pitch
                plane->pitch(1);
                break;
            case GLFW_KEY_S:   // Change pitch
                plane->pitch(0);
                break;
            case GLFW_KEY_A:   // Change yaw
                plane->yaw(1);
                break;
            case GLFW_KEY_D:   // Change yaw
                plane->yaw(0);
                break;
            case GLFW_KEY_U:   // Increase speed
                plane->changeSpeed(1);
                break;
            case GLFW_KEY_J:   // Decrease speed
                plane->changeSpeed(0);
                break;
            case GLFW_KEY_O:   // Increase heightFactor
                plane->changeHeightFactor(1);
                break;
            case GLFW_KEY_L:   // Decrease heightFactor
                plane->changeHeightFactor(0);
                break;
            case GLFW_KEY_F:   // Switch to fullscreen mode
                if (isFullScreen) {
                    glfwSetWindowMonitor(window, nullptr, 0, 0, widthImagePlaneLast, heightImagePlaneLast, 0);
                    isFullScreen = false;
                } else {
                    widthImagePlaneLast = widthImagePlane;
                    heightImagePlaneLast = heightImagePlane;
                    glfwSetWindowMonitor(window, const_cast<GLFWmonitor *>(monitor), 0, 0, mode->width, mode->height,
                                         mode->refreshRate);
                    isFullScreen = true;
                }

                break;
            default:
//                std::cout << "Pressed something unhandled." << std::endl;
                break;
        }
    }
}

void windowSizeCallback(GLFWwindow *win, int width, int height) {
    widthImagePlane = width;
    heightImagePlane = height;
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Please provide only a texture image\n");
        exit(-1);
    }

    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) {
        exit(-1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    widthImagePlane = 865;
    heightImagePlane = 865;
    win = glfwCreateWindow(widthImagePlane, heightImagePlane, "CENG477 - HW4", NULL, NULL);

    if (!win) {
        glfwTerminate();
        exit(-1);
    }
    glfwMakeContextCurrent(win);

    GLenum err = glewInit();
    if (err != GLEW_OK) {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));

        glfwTerminate();
        exit(-1);
    }

    glfwSetKeyCallback(win, keyCallback);
    glfwSetWindowSizeCallback(win, windowSizeCallback);

    monitor = glfwGetPrimaryMonitor();
    mode = const_cast<GLFWvidmode *>(glfwGetVideoMode(const_cast<GLFWmonitor *>(monitor)));


    initShaders();
    glUseProgram(idProgramShader);
    initTexture(argv[1], &widthTexture, &heightTexture);

    prepareVertexData(widthTexture, heightTexture);

    plane = new Plane();
    plane->setShaderData();

    glEnable(GL_DEPTH_TEST);
    while (!glfwWindowShouldClose(win)) {

        renderFunction();

        glfwSwapBuffers(win);
        glfwPollEvents();
        plane->move();

    }


    glfwDestroyWindow(win);
    glfwTerminate();

    return 0;
}

